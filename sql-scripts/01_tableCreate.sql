
-- Drop tables if they exist
drop table if exists superhero;
drop table if exists assistant;
drop table if exists power;

-- Create the superhero table
create table superhero (
    id serial primary key,
    name varchar(255) not null unique,
    alias varchar(255) not null,
    origin varchar(255) not null
);

-- Create the assistant table
create table assistant (
    id serial primary key,
    name varchar(255) unique
);


-- Create the power table
create table power(
    id serial primary key,
    name varchar(255),
    description varchar(255)
);