
--Insert Superhero with name, alias and origin.
insert into superhero (name, alias, origin)  values ('Elena Skyler', 'Nebula Knight', 'Starlight Galaxy');
insert into superhero (name, alias, origin) values ('Jake Thunder', 'Volt Surge', 'Electro City');
insert into superhero (name, alias, origin) VALUES ('Mia Wintershade', 'Frost Whisper', 'Icy Peaks');