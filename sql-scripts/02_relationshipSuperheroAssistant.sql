
-- Altering the assistant table
-- Adding a new column named superhero_id which will store the ID of the related superhero for each assistant
alter table assistant
add column superhero_id int
constraint fk_assistant_superhero references superhero(id);

