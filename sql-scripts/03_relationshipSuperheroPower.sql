
-- Creating a table to represent the many-to-many relationship between superheroes and their powers.
-- Each row in this table will represent a specific power that a specific superhero possesses.
create table superhero_powers(
    power_id int references power,
    superhero_id int references superhero,
    primary key (power_id, superhero_id)
);