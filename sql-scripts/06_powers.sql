-- Inserting powers and their descriptions into the power table.
INSERT INTO power (name, description) VALUES
('Super blast', 'Blasting enemies in a large radius'),
('Time Warp', 'Ability to slow down or speed up time in a localized area'),
('Mental Mirage', 'Creating illusions that trick the senses of foes'),
('Gravity Null', 'Manipulating gravitational forces in a designated space'),
('Ether Walk', 'Becoming intangible and able to phase through solid objects');


-- Associating specific powers with superhero having ID = 1.
insert into superhero_powers (power_id, superhero_id) values (1, 1);
insert into superhero_powers (power_id, superhero_id) values (3, 1);
insert into superhero_powers (power_id, superhero_id) values (4, 1);

-- Associating 'Time Warp' power with multiple superheroes.
insert into superhero_powers (power_id, superhero_id) values (2, 1);
insert into superhero_powers (power_id, superhero_id) values (2, 2);
insert into superhero_powers (power_id, superhero_id) values (2, 3);
