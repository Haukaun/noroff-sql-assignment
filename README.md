# noroff-sql-assignment
An assignment focused on learning Hibernate and SQL.


### Assignment Structure
This assignment is divided into multiple appendices, each detailing specific tasks and requirements.


#### Appendix A

Create a superhero-themed database, structure it with tables, establish relationships among these tables, and populate them with relevant data.
The sql script for this DB can be found in the folder "sql-scripts".



#### Appendix B

The aim is to use Java, coupled with your understanding of SQL and Spring, to breathe life into "Chinook". Dive deep into SQL data manipulation within Spring, leveraging the JDBC alongside the PostgreSQL driver.
