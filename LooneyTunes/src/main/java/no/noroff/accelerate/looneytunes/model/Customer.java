package no.noroff.accelerate.looneytunes.model;


/**
 * Record representing a customer with details such as name, country, postal code, phone number, and email.
 */
 public record Customer(int id, String firstName, String lastName, String country,
                       String postalCode, String phoneNumber, String email) {
}
