package no.noroff.accelerate.looneytunes.repository;

import no.noroff.accelerate.looneytunes.model.Customer;
import no.noroff.accelerate.looneytunes.model.CustomerCountry;
import no.noroff.accelerate.looneytunes.model.CustomerGenre;
import no.noroff.accelerate.looneytunes.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Implementation of the CustomerRepository interface to interact with a SQL database to
 * perform CRUD operations on the 'customer' table and related queries.
 */
 @Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    /**
     * Retrieves all customers from the database.
     *
     * @return a List of Customer objects.
     */
    @Override
    public List<Customer> findAll() {

        List<Customer> customers = new ArrayList<>();

        String sql = "SELECT * FROM customer";

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet result = statement.executeQuery();

            while (result.next()){
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );

                customers.add(customer);

            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return customers;

    }


    /**
     * Retrieves a customer by its ID.
     *
     * @param id the ID of the customer.
     * @return a Customer object or null if not found.
     */
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;

        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();

            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return customer;

    }


    /**
     * Retrieves a customer by its first name.
     *
     * @param name the first name of the customer.
     * @return a Customer object or null if not found.
     */
    @Override
    public Customer getByName(String name) {

        Customer customer = null;

        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, name);

            ResultSet result = statement.executeQuery();

            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }

        return customer;
    }


    /**
     * Retrieves a paginated list of customers.
     *
     * @param limit  the maximum number of customers to return.
     * @param offset the starting position of the first customer in the result set.
     * @return a List of Customer objects.
     */
    @Override
    public List<Customer> getPageOfCustomer(int limit, int offset) {

        List<Customer> customers = new ArrayList<>();

        String sql = "SELECT * FROM customer ORDER BY customer_id LIMIT ? OFFSET ?";

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setInt(1, limit);

            statement.setInt(2, offset);

            ResultSet result = statement.executeQuery();

            while (result.next()){
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return customers;

    }


    /**
     * Inserts a new customer into the database.
     *
     * @param customer the Customer object to be added.
     * @return the number of affected rows in the database.
     */
    @Override
    public int create(Customer customer) {

        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)";

        int result = 0;

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());

            result = statement.executeUpdate();

        } catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Updates the details of an existing customer in the database.
     *
     * @param customer the Customer object with updated details.
     * @return the number of affected rows in the database.
     */
    @Override
    public int update(Customer customer) {


        String sql = "UPDATE customer " +
                "SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? " +
                "WHERE customer_id = ?";

        int result = 0;

        try(Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());

            statement.setInt(7, customer.id());

            result = statement.executeUpdate();

        } catch (SQLException e){
            e.printStackTrace();
        }

        System.out.println(result + " Row affected.");

        return result;
    }

    /**
     * Retrieves the country with the most customers.
     *
     * @return a CustomerCountry object representing the country with the highest number of customers.
     */
    @Override
    public CustomerCountry getCountryWithMostCustomer(){

        String sql =
                "SELECT country, count(country) as amount" +
                " from customer" +
                " group by country" +
                " order by amount" +
                " Desc limit 1";

        CustomerCountry customerCountry = null;


        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet result = statement.executeQuery();

            while(result.next()){
                customerCountry = new CustomerCountry(result.getString(1), result.getInt(2));
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return customerCountry;
    }

    /**
     * Retrieves the customer who has the highest spending in the database.
     *
     * @return a CustomerSpender object representing the customer with the highest spending.
     */
    @Override
    public CustomerSpender getHighestSpendingCustomer(){
        String sql = "SELECT customer_id, first_name, last_name, sum(invoice.total) as summy" +
                " FROM customer" +
                " INNER JOIN invoice USING(customer_id)" +
                " GROUP BY first_name, customer_id, last_name" +
                " ORDER BY summy DESC LIMIT 1";

        CustomerSpender customerSpender = null;


        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet result = statement.executeQuery();

            while(result.next()){
                customerSpender = new CustomerSpender(result.getInt(1), result.getString(2), result.getString(3), result.getInt(4));
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return customerSpender;
    }

    /**
     * Retrieves the most popular genre for a specific user.
     *
     * @param id the ID of the user.
     * @return a CustomerGenre object representing the most popular genre for the given user.
     */
    @Override
    public List<CustomerGenre> getMostPopularGenreForUser(Integer id){


        List<CustomerGenre> customerGenres = new ArrayList<>();

        String sql = "SELECT customer_id, first_name, genre.name, count(*) as genre_count" +
                " FROM customer" +
                " INNER JOIN invoice USING (customer_id)" +
                " INNER JOIN invoice_line USING (invoice_id)" +
                " INNER JOIN track USING (track_id)" +
                " INNER JOIN genre USING (genre_id)" +
                " WHERE customer_id = ?" +
                " GROUP BY customer_id,genre.name, first_name" +
                " having" +
                " count(*) = (" +
                " select max(genre_count)" +
                " from (" +
                " select genre.name as genre_name, count(*) as genre_count" +
                " from customer" +
                " INNER JOIN invoice USING (customer_id)" +
                " INNER JOIN invoice_line USING (invoice_id)" +
                " INNER JOIN track USING (track_id)" +
                " INNER JOIN genre USING (genre_id)" +
                " where customer_id = ?" +
                " group by genre.name" +
                ") as tab" +
                ")" +
                " ORDER BY genre_count DESC";

        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setInt(1, id);
            statement.setInt(2, id);

            ResultSet result = statement.executeQuery();

            while(result.next()){
                CustomerGenre customerGenre = new CustomerGenre
                        (
                                result.getInt(1),
                                result.getString(2),
                                result.getString(3),
                                result.getInt(4));

                customerGenres.add(customerGenre);
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return customerGenres;
    }


}
