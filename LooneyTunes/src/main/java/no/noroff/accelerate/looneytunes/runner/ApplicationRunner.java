package no.noroff.accelerate.looneytunes.runner;


import no.noroff.accelerate.looneytunes.model.Customer;
import no.noroff.accelerate.looneytunes.model.CustomerGenre;
import no.noroff.accelerate.looneytunes.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * An application runner that demonstrates various operations of the `CustomerRepository`.
 * It showcases CRUD operations, fetching customers by name, and other specific queries.
 * This class is intended to run on application startup.
 */
@Component
public class ApplicationRunner implements CommandLineRunner {

    final CustomerRepository customerRepository;

    public ApplicationRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Get all customers");
        System.out.println("------------------");
        List<Customer> customer = customerRepository.findAll();
        for (Customer value : customer) {
            System.out.println(value);
        }


        System.out.println("------------------");
        System.out.println("Get customer by Id: ");

        Customer getCustomerById = customerRepository.findById(2);
        System.out.println(getCustomerById);

        System.out.println("------------------");
        System.out.println("Get customer by name: ");

        Customer getCustomerByName = customerRepository.getByName("Tim");
        System.out.println(getCustomerByName);

        System.out.println("---------------");
        System.out.println("Customer pages");
        System.out.println(" ");

        List<Customer> customerPage = customerRepository.getPageOfCustomer(10, 0);

        System.out.println(customerPage);

        int customerSize = customerRepository.findAll().size();

        Customer newCustomer = new Customer(customerSize + 1, "Håkon", "Sotre", "Norway", "7013", "98454796", "haakonfs@hotmail.com");
        customerRepository.create(newCustomer);

        System.out.println("--------------");
        System.out.println("New Customer:");
        System.out.println(" ");

        Customer getNewCustomerByName = customerRepository.getByName("Håkon");
        System.out.println(getNewCustomerByName);


        System.out.println("--------------");
        System.out.println("Update Customer:");
        System.out.println(" ");



        Customer updateCustomer = new Customer(newCustomer.id(), newCustomer.firstName(), newCustomer.lastName(), "Asia", newCustomer.postalCode(), newCustomer.phoneNumber(), newCustomer.email());

        customerRepository.update(updateCustomer);
        System.out.println(updateCustomer);


        System.out.println("--------------");
        System.out.println("Get Country with most Customers:");

        System.out.println(customerRepository.getCountryWithMostCustomer());

        System.out.println("--------------");
        System.out.println("Get highest spending customer:");

        System.out.println(customerRepository.getHighestSpendingCustomer());

        System.out.println("--------------");
        System.out.println("Get the most popular genre for given customer:");

        for(CustomerGenre customerGenre: customerRepository.getMostPopularGenreForUser(12)){
            System.out.println(customerGenre);
        }

    }
}
