package no.noroff.accelerate.looneytunes.repository;

import no.noroff.accelerate.looneytunes.model.Customer;
import no.noroff.accelerate.looneytunes.model.CustomerCountry;
import no.noroff.accelerate.looneytunes.model.CustomerGenre;
import no.noroff.accelerate.looneytunes.model.CustomerSpender;

import java.util.List;

/**
 * Interface for repository operations on the Customer entity.
 * Provides basic CRUD operations as well as some specific queries.
 */

 public interface CustomerRepository extends CRUDRepository<Customer, Integer> {

    Customer getByName(String name);

    List<Customer> getPageOfCustomer(int limit, int offset);

    CustomerCountry getCountryWithMostCustomer();

    CustomerSpender getHighestSpendingCustomer();

    List<CustomerGenre> getMostPopularGenreForUser(Integer id);
}
