package no.noroff.accelerate.looneytunes.model;


/**
 * Record representing a country and the number of customers associated with that country.
 */
 public record CustomerCountry(String country, int customerAmount) {
}
