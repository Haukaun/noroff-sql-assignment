package no.noroff.accelerate.looneytunes.model;

/**
 * Record representing a customer's name, their country, and the count of a specific genre associated with them.
 */
 public record CustomerGenre(int customerId ,String customerName, String country, int genreCount) {
}
