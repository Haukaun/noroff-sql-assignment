package no.noroff.accelerate.looneytunes.model;

/**
 * Record representing a customer's name and the sum they have spent.
 */
 public record CustomerSpender(int customerId, String firstname, String lastName , int sum) {

}
