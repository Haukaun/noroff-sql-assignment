package no.noroff.accelerate.looneytunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LooneyTunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LooneyTunesApplication.class, args);
	}

}
