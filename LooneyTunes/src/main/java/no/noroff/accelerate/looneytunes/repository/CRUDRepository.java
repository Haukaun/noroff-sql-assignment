package no.noroff.accelerate.looneytunes.repository;

import java.util.List;


/**
 * A generic interface for CRUD (Create, Read, Update, Delete) operations.
 * Provides basic operations on entities of type {@code T} with an ID of type {@code U}.
 *
 * @param <T> The type of the entity.
 * @param <U> The type of the entity's identifier.
 *
 */
 public interface CRUDRepository<T, U> {
    List<T> findAll();
    T findById(U id);
    int create(T object);
    int update(T object);

}
